/*
 * WinDraw.h
 *
 *  Created on: 22 мая 2018 г.
 *      Author: anton
 */

#ifndef WINDRAW_H_
#define WINDRAW_H_

#include <stdint.h>

#include <X11/Xlib.h>



class WinDraw{

public:
	WinDraw(unsigned width, unsigned height, unsigned bpp);
	~WinDraw();

	void Draw(const uint8_t* pixels);
	bool CheckExit();

private:

	unsigned width, height, bpp;

	Display* display;		/* pointer to X Display structure.           */
	GC gc;			/* GC (graphics context) used for drawing    */
	Window win;

	int display_width;
	int display_height;
    int screen_num;

    Window create_window(Display* display, int width, int height, int x, int y);
    GC create_gc(Display* display, Window win);

};


#endif /* WINDRAW_H_ */
