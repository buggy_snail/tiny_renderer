/*
 * main.cpp
 *
 *  Created on: 22 мая 2018 г.
 *      Author: anton
 */

#include <unistd.h>
#include <time.h>

#include "tgaimage.h"
#include "WinDraw.h"

const TGAColor white = TGAColor(255, 255, 255, 255);
const TGAColor red   = TGAColor(255, 0,   0,   255);


#define WIN_WIDTH   640
#define WIN_HEIGHT  480


int main(int argc, char** argv) {

	WinDraw w(WIN_WIDTH, WIN_HEIGHT, TGAImage::RGB);

	TGAImage image(WIN_WIDTH, WIN_HEIGHT, TGAImage::RGB);
	image.set(52, 41, red);
	image.flip_vertically(); // i want to have the origin at the left bottom corner of the image

	w.Draw(image.buffer());


    while(!w.CheckExit()){
	   usleep(100000);
    }

	//image.write_tga_file("output.tga");
	return 0;
}


