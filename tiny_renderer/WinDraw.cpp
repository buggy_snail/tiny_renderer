/*
 * WinDraw.cpp
 *
 *  Created on: 22 мая 2018 г.
 *      Author: anton
 */

#include "WinDraw.h"


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <time.h>


WinDraw::WinDraw(unsigned x, unsigned y, unsigned bpp){

	width = x;
	height = y;
	this->bpp = bpp;


	int screen_num;		/* number of screen to place the window on.  */
	Window win;			/* pointer to the newly created window.      */
	unsigned int display_width,	display_height;	/* height and width of the X display.        */
	char *display_name = getenv("DISPLAY");  /* address of the X display.      */

	/*  in our window.			     */

	/* open connection with the X server. */
	display = XOpenDisplay(display_name);
	if (display == NULL) {
		fprintf(stderr, "cannot connect to X server '%s'\n", display_name);
		return;
	}

	/* get the geometry of the default screen for our display. */
	screen_num = DefaultScreen(display);
	display_width = DisplayWidth(display, screen_num);
	display_height = DisplayHeight(display, screen_num);


	/* create a simple window, as a direct child of the screen's   */
	/* root window. Use the screen's white color as the background */
	/* color of the window. Place the new window's top-left corner */
	/* at the given 'x,y' coordinates.                             */
	win = create_window(display, width, height, (display_width-width)/2, (display_height-height)/2);

	/* allocate a new GC (graphics context) for drawing in the window. */
	gc = create_gc(display, win);
	XSync(display, false);

	usleep(200000);

}

WinDraw::~WinDraw(){
	XDestroyWindow(display, win);
	XCloseDisplay(display);
}


Window WinDraw::create_window(Display* display, int width, int height, int x, int y){

  int screen_num = DefaultScreen(display);
  int win_border_width = 2;

  /* create a simple window, as a direct child of the screen's */
  /* root window. Use the screen's black and white colors as   */
  /* the foreground and background colors of the window,       */
  /* respectively. Place the new window's top-left corner at   */
  /* the given 'x,y' coordinates.                              */
  win = XCreateSimpleWindow(display, RootWindow(display, screen_num),
                            x, y, width, height, win_border_width,
                            BlackPixel(display, screen_num),
							BlackPixel(display, screen_num));

  /* make the window actually appear on the screen. */
  XMapWindow(display, win);

  /* flush all pending requests to the X server. */
  XFlush(display);

  return win;
}


GC WinDraw::create_gc(Display* display, Window win)
{
  GC gc;				/* handle of newly created GC.  */

  unsigned long valuemask = 0;		/* which values in 'values' to  */
					/* check when creating the GC.  */
  XGCValues values;			/* initial values for the GC.   */

#if 0
  unsigned int line_width = 2;		/* line width for the GC.       */
  int line_style = LineSolid;		/* style for lines drawing and  */
  int cap_style = CapButt;		/* style of the line's edje and */
  int join_style = JoinBevel;		/*  joined lines.		*/
#endif

  int screen_num = DefaultScreen(display);

  gc = XCreateGC(display, win, valuemask, &values);
  if (gc < 0) {
	fprintf(stderr, "XCreateGC: \n");
  }

  /* allocate foreground and background colors for this GC. */
  XSetForeground(display, gc, WhitePixel(display, screen_num));
  XSetBackground(display, gc, BlackPixel(display, screen_num));


#if 0
  /* define the style of lines that will be drawn using this GC. */
  XSetLineAttributes(display, gc, line_width, line_style, cap_style, join_style);

  /* define the fill style for the GC. to be 'solid filling'. */
  XSetFillStyle(display, gc, FillSolid);
#endif

  return gc;
}


void WinDraw::Draw(const uint8_t* pixels){

	unsigned bytes;

	uint32_t color=0;
	uint8_t* color_ptr=reinterpret_cast<uint8_t*>(&color);


	for(unsigned y=0; y<height; y++)
		for(unsigned x=0; x<width; x++, bytes=0, color=0, color_ptr = reinterpret_cast<uint8_t*>(&color)){

			while(bytes++<bpp)
				*color_ptr++=*pixels++;

	        XSetForeground(display, gc, color);
	        XDrawPoint(display, win, gc, x, y);
		}


	XFlush(display);

}

bool WinDraw::CheckExit(){
	XEvent xeEvent;

	XNextEvent(display, &xeEvent);

	return (xeEvent.type == DestroyNotify);
}
